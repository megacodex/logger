<?php
/***********************************************************************************************************************
 * @author	Zahirul Hasan<hasan@megacodex.com>
 * @copyright Copyright (c) 2011 - 2018 @ Mega Codex (https://megacodex.com)
 * @license   https://megacodex.com/license-agreement
 **********************************************************************************************************************/
namespace MegaCodex\Logger\Formatter;

/**
 * Formats incoming records into an HTML table
 *
 * This is especially useful for html email logging
 *
 * @author Tiago Brito <tlfbrito@gmail.com>
 */
class HtmlFormatter extends \Monolog\Formatter\HtmlFormatter
{
	protected $logLevels = [
		\MegaCodex\Logger\Logger::UNKNOWN     => "#70FF00",
		\MegaCodex\Logger\Logger::DEPRECATED => "#70FF00",
		\MegaCodex\Logger\Logger::DEBUG     => "#70FF00",
		\MegaCodex\Logger\Logger::INFO      => "#70FF00",
		\MegaCodex\Logger\Logger::NOTICE    => "#B8FF00",
		\MegaCodex\Logger\Logger::WARNING   => "#FFC800",
		\MegaCodex\Logger\Logger::ERROR     => "#FF8000",
		\MegaCodex\Logger\Logger::CRITICAL  => "#FF6500",
		\MegaCodex\Logger\Logger::ALERT     => "#FF2F00",
		\MegaCodex\Logger\Logger::EMERGENCY => "#FF0000",
	];

	/**
	 * Formats a log record.
	 *
	 * @param  array $record A record to format
	 * @return mixed The formatted record
	 */
	public function format(array $record): string
	{
		$output = parent::format($record);
		$output .= "<style>body{padding:5px;}</style>";
		return $output;
	}

	/**
	 * Create a HTML h1 tag
	 *
	 * @param  string $title Text to be in the h1
	 * @param  int    $level Error level
	 * @return string
	 */
	protected function addTitle(string $title, int $level)
	{
		$title = htmlspecialchars($title, ENT_NOQUOTES, "UTF-8");

		return "<h1 style=\"background: ".$this->logLevels[$level].";color: #555;padding: 5px;\">".$title."</h1>";
	}

	/**
	 * Creates an HTML table row
	 *
	 * @param  string $th       Row header content
	 * @param  string $td       Row standard cell content
	 * @param  bool   $escapeTd false if td content must not be html escaped
	 * @return string
	 */
	protected function addRow(string $th, string $td = " ", bool $escapeTd = true): string
	{
		$th = htmlspecialchars($th, ENT_NOQUOTES, "UTF-8");
		if ($escapeTd) {
			$td = htmlspecialchars($td, ENT_NOQUOTES, "UTF-8");
		}

		return "<tr style=\"padding: 4px;spacing: 0;text-align: left;\">\n<th style=\"background: #cccccc\" width=\"100px\">$th:</th>\n<td style=\"padding: 4px;spacing: 0;text-align: left;background: #eeeeee\"><pre style=\"white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;\">".$td."</pre></td>\n</tr>";
	}
}
