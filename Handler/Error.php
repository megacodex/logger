<?php

/***********************************************************************************************************************
 * @author	Zahirul Hasan<hasan@megacodex.com>
 * @copyright Copyright (c) 2011 - 2018 @ Mega Codex (https://megacodex.com)
 * @license   https://megacodex.com/license-agreement
 **********************************************************************************************************************/

namespace MegaCodex\Logger\Handler;

class Error extends Base
{
	/**
	 * @var string
	 */
	protected $fileName = "error.log";

	/**
	 * @var int
	 */
	protected $loggerType = \MegaCodex\Logger\Logger::ERROR;
}
