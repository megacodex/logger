<?php
/***********************************************************************************************************************
 * @author	Zahirul Hasan<hasan@megacodex.com>
 * @copyright Copyright (c) 2011 - 2018 @ Mega Codex (https://megacodex.com)
 * @license   https://megacodex.com/license-agreement
 **********************************************************************************************************************/
namespace MegaCodex\Logger\Handler;

class Base extends \Monolog\Handler\StreamHandler
{
	/**
	 *
	 * @var bool
	 */
	protected $isDeveloperMode;
	
	/**
	 * @var string
	 */
	protected $fileName;

	/**
	 * @var int
	 */
	protected $loggerType = \MegaCodex\Logger\Logger::ERROR;

	/**
	 * @param DriverInterface $filesystem
	 * @param string $filePath
	 * @param string $fileName
	 */
	public function __construct(
		$filePath,
		\MegaCodex\Logger\Handler\Exception $exceptionHandler = null
	) {
		if (!empty($this->fileName)) {
			$this->fileName = $this->sanitizeFileName($this->fileName);
		}
		if (!$this instanceof \MegaCodex\Logger\Handler\Exception) {
			$this->exceptionHandler = empty($exceptionHandler) ? new \MegaCodex\Logger\Handler\Exception($filePath) : $exceptionHandler;
		}

		parent::__construct(
			$filePath . $this->fileName,
			$this->loggerType,
			false
		);

		$this->setFormatter(new \Monolog\Formatter\LineFormatter(null, null, true));
	}
	
	/**
	 * @param bool $mode
	 */
	public function setDeveloperMode(bool $mode = true)
	{
		$this->isDeveloperMode = boolval($mode);
		$this->setFormatter(new \MegaCodex\Logger\Formatter\HtmlFormatter(null, null, true));
	}

	/**
	 * @param string $fileName
	 *
	 * @return string
	 * @throws \InvalidArgumentException
	 */
	private function sanitizeFileName($fileName)
	{
		if (!is_string($fileName)) {
			throw new \InvalidArgumentException("Filename expected to be a string");
		}

		$parts	 = explode("/", $fileName);
		$parts	 = array_filter(
			$parts,
			function ($value) {
				return !in_array($value, ["", ".", ".."]);
			}
		);

		return implode("/", $parts);
	}

	public function write(array $record)
	{
		if (isset($record["context"]["exception"])
				&& !$this instanceof \MegaCodex\Logger\Handler\Exception) {
			$this->exceptionHandler->handle($record);
			return;
		}

		$record["formatted"] = $this->getFormatter()->format($record);
		if ($this->isDeveloperMode) {
			print("<pre>".$record["formatted"]."<pre>");
		} else {
			parent::write($record);
		}
	}
}
