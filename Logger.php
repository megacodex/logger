<?php
/***********************************************************************************************************************
 * @author	Zahirul Hasan<hasan@megacodex.com>
 * @copyright Copyright (c) 2011 - 2018 @ Mega Codex (https://megacodex.com)
 * @license   https://megacodex.com/license-agreement
 **********************************************************************************************************************/
namespace MegaCodex\Logger;

class Logger extends \Monolog\Logger
{
	/**
	 * Detailed debug information
	 */
	const DEPRECATED = 26;
	/**
	 * Detailed debug information
	 */
	const UNKNOWN = 0;

	protected static $registeredErrorHandler = false;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(string $logDir = "")
	{
		$logDir = empty($logDir) ? $logDir : $logDir.DIRECTORY_SEPARATOR;
		$exceptionHandler	 = new \MegaCodex\Logger\Handler\Exception($logDir);
		$handlers			 = [
			"error"		 => new \MegaCodex\Logger\Handler\Error($logDir, $exceptionHandler),
			"debug"		 => new \MegaCodex\Logger\Handler\Debug($logDir, $exceptionHandler),
			"deprecated"		 => new \MegaCodex\Logger\Handler\Deprecated($logDir, $exceptionHandler),
			"unknown"		 => new \MegaCodex\Logger\Handler\Unknown($logDir, $exceptionHandler),
			"exception"	 => $exceptionHandler,
		];

		parent::__construct("MegaLogger", $handlers);
		parent::$levels[self::DEPRECATED] = "DEPRECATED";
		parent::$levels[self::UNKNOWN] = "UNKNOWN";
	}

	/**
	 * Adds a log record.
	 *
	 * @param integer $level The logging level
	 * @param string $message The log message
	 * @param array $context The log context
	 * @return Boolean Whether the record has been processed
	 */
	private function _addRecord(int $level, $message, array $context = []): bool
	{
		/**
		 * To preserve compatibility with Exception messages.
		 * And support PSR-3 context standard.
		 *
		 * @link http://www.php-fig.org/psr/psr-3/#context PSR-3 context standard
		 */
		if (is_subclass_of($message, \Exception::class) && !isset($context["exception"])) {
			$context["exception"] = $message;
		}

		$message = (string)$message;

		return parent::addRecord($level, $message, $context);
	}


	/**
	 * @param bool $mode
	 */
	public function setDeveloperMode(bool $mode = null)
	{
		$mode = boolval($mode);
		foreach ($this->handlers as $handler) {
			$handler->setDeveloperMode($mode);
		}
	}

	/** @inheritdoc */
	public function log($level, $message, array $context = [])
	{
		$level = static::toMonologLevel($level);

		$this->_addRecord($level, $message, $context);
	}

	/** @inheritdoc */
	public function debug($message, array $context = [])
	{
		$this->_addRecord(static::DEBUG, $message, $context);
	}

	/** @inheritdoc */
	public function info($message, array $context = [])
	{
		$this->_addRecord(static::INFO, $message, $context);
	}

	/** @inheritdoc */
	public function notice($message, array $context = [])
	{
		$this->_addRecord(static::NOTICE, $message, $context);
	}

	/** @inheritdoc */
	public function warning($message, array $context = [])
	{
		$this->_addRecord(static::WARNING, $message, $context);
	}

	/** @inheritdoc */
	public function error($message, array $context = [])
	{
		$this->_addRecord(static::ERROR, $message, $context);
	}

	/** @inheritdoc */
	public function critical($message, array $context = [])
	{
		$this->_addRecord(static::CRITICAL, $message, $context);
	}

	/** @inheritdoc */
	public function alert($message, array $context = [])
	{
		$this->_addRecord(static::ALERT, $message, $context);
	}

	/** @inheritdoc */
	public function emergency($message, array $context = [])
	{
		$this->_addRecord(static::EMERGENCY, $message, $context);
	}

	/**
	 * Register logging system as an error handler to log PHP errors
	 *
	 * @return mixed  Returns result of set_error_handler
	 * @throws Exception\InvalidArgumentException if logger is null
	 */
	public static function setPhpErrorHandler(\MegaCodex\Logger\Logger $logger)
	{
		// Only register once per instance
		if (static::$registeredErrorHandler) {
			return false;
		}
		$errorPriorityMap = [
			E_NOTICE            => self::NOTICE,
			E_USER_NOTICE       => self::NOTICE,
			E_WARNING           => self::WARNING,
			E_CORE_WARNING      => self::WARNING,
			E_USER_WARNING      => self::WARNING,
			E_ERROR             => self::ERROR,
			E_USER_ERROR        => self::ERROR,
			E_CORE_ERROR        => self::ERROR,
			E_RECOVERABLE_ERROR => self::ERROR,
			E_PARSE             => self::ERROR,
			E_COMPILE_ERROR     => self::ERROR,
			E_COMPILE_WARNING   => self::ERROR,
			E_STRICT            => self::DEBUG,
			E_DEPRECATED        => self::DEPRECATED,
			E_USER_DEPRECATED   => self::DEPRECATED,
		];

		$previous = set_error_handler(
			function ($level, $message, $file, $line) use ($logger, $errorPriorityMap) {
				$iniLevel = error_reporting();
				if ($iniLevel & $level) {
					if (isset($errorPriorityMap[$level])) {
						$priority = $errorPriorityMap[$level];
					} else {
						$priority = \MegaCodex\Logger\Logger::INFO;
					}
					$logger->log($priority, $message, [
						"errno"   => $level,
						"file"    => $file,
						"line"    => $line,
					]);
				}
			}
		);

		register_shutdown_function(function () use ($logger, $errorPriorityMap) {
			$error = error_get_last();
			if (null === $error || ! in_array($error["type"], [
				E_ERROR,
				E_PARSE,
				E_CORE_ERROR,
				E_CORE_WARNING,
				E_COMPILE_ERROR,
				E_COMPILE_WARNING
			], true)
			) {
				return;
			}
			$logger->log(
				$errorPriorityMap[$error["type"]],
				$error["message"],
				[
					"file" => $error["file"],
					"line" => $error["line"],
				]
			);
		});

		set_exception_handler(function ($exception) use ($logger, $errorPriorityMap) {
			do {
				$priority = self::ERROR;
				if ($exception instanceof \ErrorException && isset($errorPriorityMap[$exception->getSeverity()])) {
					$priority = $errorPriorityMap[$exception->getSeverity()];
				}
				$logger->log($priority, $exception);

				$exception = $exception->getPrevious();
			} while ($exception);
		});

		static::$registeredErrorHandler = true;
		return $previous;
	}
	/**
	 * Unregister error handler
	 *
	 */
	public static function unregisterErrorHandler()
	{
		restore_error_handler();
		restore_exception_handler();
		static::$registeredErrorHandler = false;
	}
}
